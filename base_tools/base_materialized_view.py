from sqlalchemy import text

from base_tools.base_model import BaseModel
from base_tools.utils import get_session


class BaseMaterializedViewModel(BaseModel):
    __abstract__ = True

    @classmethod
    def refresh(cls, concurrently=True):
        _concurrently = "CONCURRENTLY" if concurrently else ""
        with get_session(autocommit=True) as session:
            session.execute(text(f"REFRESH MATERIALIZED VIEW {_concurrently} {cls.__tablename__}"))

