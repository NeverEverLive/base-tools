class EngineRegistry:
    __slots__ = ("fnc", "args", "kwargs")

    def __new__(cls, fnc, *args, **kwargs):
        cls.fnc = fnc
        cls.args = args
        cls.kwargs = kwargs
        return cls

    @classmethod
    def create_engine(cls):
        return cls.fnc(*cls.args, **cls.kwargs)
