from typing import List
from typing import Type
import sqlalchemy

version, _, _ = sqlalchemy.__version__.split(".")

if version == "1":
    from sqlalchemy.ext.declarative import declarative_base
    DeclarativeBase = declarative_base()

if version == "2":
    from sqlalchemy.orm import DeclarativeBase

from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import Session


class BaseModel(DeclarativeBase):
    """Postgres base model"""

    _session: scoped_session[Session] | None = None

    @classmethod
    def set_session(cls, session: scoped_session[Session]) -> None:
        cls._session = session

    @classmethod
    def first(cls) -> "Type[BaseModel]":
        return cls._session.query(cls).first()

    @classmethod
    def all(cls) -> "List[Type[BaseModel]]":
        return cls._session.query(cls).all()

    @classmethod
    def fill(cls, **data) -> "BaseModel":
        obj = cls()
        for key, value in data.items():
            setattr(obj, key, value)
        return obj
