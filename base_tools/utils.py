from contextlib import contextmanager, asynccontextmanager
from typing import AsyncContextManager, ContextManager
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Session
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

from base_tools.base_model import BaseModel
from base_tools.engine_registry import EngineRegistry
from base_tools.proxy import TLS, Proxy


@asynccontextmanager
async def get_async_session() -> AsyncContextManager[AsyncSession]:
    engine = EngineRegistry.create_engine()
    session = AsyncSession(engine)
    try:
        yield session
    finally:
        await session.close()


@contextmanager
def get_session(autocommit=False) -> ContextManager[Session]:
    engine = EngineRegistry.create_engine()
    session = Session(engine)
    try:
        yield session
    finally:
        if autocommit:
            session.commit()
        session.close()


def set_session():
    """Создать сессию"""
    engine = EngineRegistry.create_engine()
    db_session = scoped_session(sessionmaker(autoflush=True, bind=engine))
    BaseModel.set_session(db_session)
    BaseModel.query = db_session.query_property()

def drop_all():
    engine = EngineRegistry.create_engine()
    BaseModel.metadata.drop_all(engine)


set_session()
_tls = TLS()


def _get_current_session():
    return _tls.current_session


def _set_current_session(session):
    _tls.current_session = session


def get_current_session():
    return _get_current_session()


current_session = Proxy(get_current_session)
